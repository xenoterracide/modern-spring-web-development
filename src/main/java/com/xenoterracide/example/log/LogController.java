package com.xenoterracide.example.log;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( path = "logs" )
class LogController {

    private final LogService logService;
    private final LogRepository repository;

    @Autowired
    LogController( final LogService logService, final LogRepository repository ) {
        this.logService = logService;
        this.repository = repository;
    }

    @RequestMapping( method = RequestMethod.GET, path = "create" )
    Iterable<Log> createLogs() {
        LoggerFactory.getLogger( this.getClass() ).debug( "creating logs" );
        return logService.createLogs();
    }

    @RequestMapping( method = RequestMethod.GET, path = "jpql" )
    Page<Log> getLogsByJpql( final Pageable pageable ) {
        return repository.findAllCustomJpql( pageable );
    }

    @RequestMapping( method = RequestMethod.GET )
    Page<Log> getLogs( final Pageable pageable ) {
        return repository.findAllByOrderByCreatedDescEntryAsc( pageable );
    }

    @RequestMapping( method = RequestMethod.GET, path = "spec" )
    Page<Log> getLogsBySpecification( final Pageable pageable ) {
        return repository.findAll( ( root, query, cb ) -> {
            query.orderBy(
                    cb.desc( root.get( "created" ) ),
                    cb.asc( root.get( "entry" ) )
            );

            query.groupBy( root.get( "id" ) );
            return null;
        }, pageable );
    }
}
