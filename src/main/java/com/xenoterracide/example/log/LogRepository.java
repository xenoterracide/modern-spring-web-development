package com.xenoterracide.example.log;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

interface LogRepository extends PagingAndSortingRepository<Log, Long>, JpaSpecificationExecutor<Log> {

    @Query( "select l from Log l order by l.created desc, l.entry asc " )
    Page<Log> findAllCustomJpql( Pageable pageable );

    Page<Log> findAllByOrderByCreatedDescEntryAsc( Pageable pageable );
}
