package com.xenoterracide.example.log;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Date;

@Entity
public class Log extends AbstractPersistable<Long> {

    @NotNull
    @Column( nullable = false )
    @Temporal( TemporalType.TIMESTAMP )
    private Date created;

    @NotNull
    @Column( nullable = false )
    private String entry;


    public Instant getCreated() {
        return created.toInstant();
    }

    public void setCreated( final Instant created ) {
        this.created = Date.from( created );
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry( String entry ) {
        this.entry = entry;
    }
}
