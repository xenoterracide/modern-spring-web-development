package com.xenoterracide.example.log;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static org.slf4j.LoggerFactory.getLogger;

@Service
@Transactional
public class LogService {

    private final LogRepository repository;
    private final ObjectFactory<Instant> now;
    private final Supplier<String> stringSupplier;

    @Autowired
    LogService(
            final LogRepository repository,
            final ObjectFactory<Instant> now,
            final Supplier<String> stringSupplier ) {
        this.repository = repository;
        this.now = now;
        this.stringSupplier = stringSupplier;
    }

    @Transactional
    public Iterable<Log> createLogs() {
        List<Log> logs = new ArrayList<>();
        for ( int i = 0; i < 5; i++ ) { // gross example code
            Log log = new Log();
            log.setEntry( stringSupplier.get() );
            log.setCreated( now.getObject() );
            logs.add( log );
        }
        Iterable<Log> save = repository.save( logs );

        save.forEach( ( log ) -> {
            getLogger( this.getClass() ).debug( "{} {}", log.getId(), log.getCreated() );
        } );

        return save;
    }
}
