package com.xenoterracide.example.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface Logged {
    default Logger log() {
        return LoggerFactory.getLogger( this.getClass() );
    }
}
