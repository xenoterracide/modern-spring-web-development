package com.xenoterracide.example.greeting;

import java.time.Instant;

public class Greeting {

    private final String content;
    private final Instant time;

    public Greeting( final String content, final Instant time ) {

        this.content = content;
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public Instant getTime() {
        return time;
    }
}
