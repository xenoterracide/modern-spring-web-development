package com.xenoterracide.example.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
class GreetingController {

    private static final String template = "Hello, %s";
    private final CounterService counterService;

    @Autowired
    GreetingController( final CounterService counterService ) {
        this.counterService = counterService;
    }

    @RequestMapping( "/greeting" )
    Greeting greeting( @RequestParam( value = "name", defaultValue = "World" ) String name ) {
        counterService.increment( "greeting.count." + name );
        return new Greeting( String.format( template, name ), Instant.now() );
    }
}
