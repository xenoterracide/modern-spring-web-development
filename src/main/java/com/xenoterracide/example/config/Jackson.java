package com.xenoterracide.example.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
class Jackson {

    @Autowired
    void configureObjectMapper( final ObjectMapper objectMapper ) {
        objectMapper.disable( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS );
    }
}
