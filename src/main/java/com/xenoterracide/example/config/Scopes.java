package com.xenoterracide.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.SimpleTransactionScope;

@Configuration
class Scopes {
    public static final String TXN = "transaction";

    @Autowired
    void configureScopes( ConfigurableBeanFactory beanFactory ) {
        beanFactory.registerScope( TXN, new SimpleTransactionScope() );
    }
}
