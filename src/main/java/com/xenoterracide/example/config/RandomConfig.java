package com.xenoterracide.example.config;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;
import java.util.function.Supplier;

@Configuration
class RandomConfig {

    @Bean
    Supplier<String> randomString() {
        return () -> RandomStringUtils.randomAlphabetic( 10 );
    }

    @Bean
    Supplier<Integer> randomInt() {
        return () -> new Random().nextInt();
    }
}
