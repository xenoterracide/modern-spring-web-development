package com.xenoterracide.example.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.time.Instant;
import java.util.Date;

@Configuration
class Time {

    @Bean
    @Scope( value = Scopes.TXN )
    Instant instantNow() {
        return Instant.now();
    }

    @Bean
    @Scope( value = Scopes.TXN )
    Date dateNow() {
        return new Date();
    }
}
