package com.xenoterracide.example.todo;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class ToDo extends AbstractPersistable<Long> {

    @NotNull
    @Column( nullable = false )
    private String task;

    @NotNull
    @Column( nullable = false )
    private Boolean completed;

    public String getTask() {
        return task;
    }

    public void setTask( String task ) {
        this.task = task;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted( Boolean completed ) {
        this.completed = completed;
    }
}
