package com.xenoterracide.example.todo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource( collectionResourceRel = "/todos", path = "/todos" )
public interface ToDoRepository extends PagingAndSortingRepository<ToDo, Long> {
    Page<ToDo> findAllByTaskLike( @Param( "task" ) String task, Pageable pageable );

    Page<ToDo> findAllByCompleted( @Param( "completed" ) Boolean completed, Pageable pageable );

    Optional<Long> findById( Long id );
}
